---
title: "How to Export Fusion 360 files to Other File Types"
date: 2019-12-09T21:56:14-07:00
image: https://i.imgur.com/q7V9tC5.jpg
author: Anson Biggs
---

A major downfall of Fusion 360 is its lack of exporting options, and its main filetype .f3d seems to be closed source and isn't supported by any other programs. Processes like exporting to a .step or .obj then using something else like Autodesk Inventor or Blender then exporting to your final filetype from those programs can sometimes cause artifacts, loss of dimensions, and can take a very long time to convert. Luckily Fusion has more export options built in, and they don't show the functionality in their desktop program.

---

## [Go to the Autodesk 360 Dashboard](https://myhub.autodesk360.com)

Going to [https://myhub.autodesk360.com](https://myhub.autodesk360.com) will give you a similar looking Project and file options as the data panel in the Desktop version. Navigate to your model and open it. Whether you are in the Design View or the 3D View in the top right corner, you'll have a few options to choose from.
Clicking the download button provides you with a ton more options than the desktop version offers. Unfortunately, you cannot download straight from the website as it takes time to convert, so you have to wait for an email. In my experience, this usually takes less than a minute although I would imagine more complex models take more time.
Below I have a list of every file-type that can be exported and a quick summary of what they are good for.

## Fusion 360 Export Formats

_If you have anything to add to this table please contact me._

<table style="white-space: normal;">
  <tr>
    <th>Format<br></th>
    <th>What its good for.<br></th>
  </tr>
  <tr>
    <td>Fusion 360 Archive</td>
    <td>Provides a .f3d file-type which is a proprietary file-type for Fusion 360. Preserves full model functionality when opened in any Fusion 360, and is a great way to make personal backups of Fusion Models.</td>
  </tr>
  <tr>
    <td>Inventor 2016</td>
    <td>Downloads either a single .ipt if the part is a single component, or multiple .ipt files and a .iam if your Fusion model contains multiple components. .ipt is an Inventor Part file, and .iam is an Inventor Assembly file. The models lose all parametric abilities, but keep dimensions and shape.</td>
  </tr>
  <tr>
    <td>IGES</td>
    <td>.iges is an open CAD format and should be usable in any CAD software. Does not retain any parametric elements</td>
  </tr>
  <tr>
    <td>SAT</td>
    <td>Developed by Dassault Systems but is a pretty widely accepted file-type for CAD software. Does not retain any parametric elements.</td>
  </tr>
  <tr>
    <td>SMT</td>
    <td>Only usable by Fusion 360 and Inventor 2019</td>
  </tr>
  <tr>
    <td>STEP</td>
    <td>.step is an open CAD format and should be usable in any CAD software. Does not retain any parametric elements</td>
  </tr>
  <tr>
    <td>DWG</td>
    <td>Autodesk AutoCAD file format. Exports a wireframe of your model. Great for exporting to AutoCAd but otherwise useless.</td>
  </tr>
  <tr>
    <td>DXF</td>
    <td>Drawing format. Exports a wireframe of your model. Much better off making an actual drawing in fusion then exporting a .dxf</td>
  </tr>
  <tr>
    <td>STL</td>
    <td>.stl is an open standard used by most 3D modeling and CAD programs and is great for 3D printing, keep in mind that units are not stored in .stl formats, so it is important to remember what units your model was created in. If you aren't prompted by the soft then it likely assumed millimeters so you may need to convert yourself by scaling the model.</td>
  </tr>
  <tr>
    <td>FBX</td>
    <td>Autodesk owned file format. No real use in CAD and doesn't preserve dimensions so you end up with a tiny model.</td>
  </tr>
  <tr>
    <td>SketchUp</td>
    <td>Exports a .skp which opens in SketchUp perfectly, Great if you want to use your part in a SketchUp Project.</td>
  </tr>
  <tr>
    <td>OBJ</td>
    <td>.obj or Object is an open format and the geometry should be workable in most 3D modeling or CAD programs. </td>
  </tr>
</table>
